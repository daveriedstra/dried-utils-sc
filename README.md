
dried-utils-sc
====

SuperCollider extension
----

This is my personal SuperCollider extension. Like [dried-utils](https://gitlab.com/daveriedstra/dried-utils) for Pure Data, this is a little bag-o-tricks developed for and by my workflow, which means it might change. Feel free to use. Documentation can be found in each class's helpfile.

### Classes

- `ConstrainByOctave` - constrain a frequency to a range by modifying its octave
- `Crossing`, `CrossingUp`, and `CrossingDown` - determine when two streams cross
- `Crow` - converse with monome crow (see also [superCrow](https://github.com/williamthazard/superCrow/tree/main) and [discussion](https://llllllll.co/t/supercrow/63786))
- `LInterpolate` - linearly interpolate between an array of (streams of) values, wrapping at the edges

### Class Extensions

- `aliasedRing` in [`SerialOSCEnc`](https://github.com/antonhornquist/SerialOSCClient-sc) - creates an array of aliased positions suitable for `ringMap`
