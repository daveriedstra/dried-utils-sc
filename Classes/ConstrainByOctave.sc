// constrains a freq into a range by lowering / raising its octave

ConstrainByOctave {
	*constrain {
		arg freq, min = 0, max = 5000;
		while { freq > max } { freq = freq / 2 };
		while { freq < min } { freq = freq * 2 };
		^freq
	}

	// b0rked: can't use control structures in stream ("Non Boolean in test")
	// *kr {
	// 	arg freq, min = 0, max = freq * 2;
	// 	^ConstrainByOctave.constrain(freq, min, freq)
	// }
}
