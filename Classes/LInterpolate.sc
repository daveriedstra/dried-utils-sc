// linearly interpolate between streams in an array
// with wrapping
// Like LinSelectX except wrapping works :O

LInterpolate {
	// static function
	*interpolate {
		arg srcs, i;
		var i_left = i.floor % srcs.size,
			i_right = i_left + 1 % srcs.size,
			val_right = i.frac,
			val_left = 1 - val_right;

		^(srcs[i_left] * val_left) + (srcs[i_right] * val_right)
	}

	*ar {
		arg srcs, i;
		var i_left = i.floor % srcs.size,
			i_right = (i_left + 1) % srcs.size;
		var left = Select.ar(i_left, srcs);
		var right = Select.ar(i_right, srcs);
		^LinXFade2.ar(left, right, i.frac * 2 - 1)
	}

	*kr {
		arg srcs, i;
		var i_left = i.floor % srcs.size,
			i_right = (i_left + 1) % srcs.size;
		var left = Select.kr(i_left, srcs);
		var right = Select.kr(i_right, srcs);
		^LinXFade2.kr(left, right, i.frac * 2 - 1)
	}
}
