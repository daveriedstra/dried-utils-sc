// A stream (a) crosses another stream (b)
// if it crosses upwards or downwards

Crossing {
	*ar {
		arg a, b;
		^(CrossingUp.ar(a,b) + CrossingDown.ar(a,b)) > 0
	}

	*kr {
		arg a, b;
		^(CrossingUp.kr(a,b) + CrossingDown.kr(a,b)) > 0
	}
}
