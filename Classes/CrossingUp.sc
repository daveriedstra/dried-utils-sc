// A stream (a) crosses another stream (b) upwards if
// - it was lesser than or equal to the second stream in the previous sample
// - and it is greater than the second stream's current sample

CrossingUp {
	*ar {
		arg a, b;
		^(Delay1.ar(a <= b)) * (a > b)
	}

	*kr {
		arg a, b;
		^(Delay1.kr(a <= b)) * (a > b)
	}
}
