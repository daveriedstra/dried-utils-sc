// Strategy for dealing with overlapping LED values
LEDMixStrategy {
	// use the greatest value
	const <greatest = 0;
	// use the lowest value
	const <least = 1;
	// sum the values
	const <sum = 2;
	// use the latest defined value
	const <last = 3;
	// use the earliest defined value
	// const <first = 4; // not implemented...
}
