// Creates an array with aliased positions suitable for ringMap
// points is a list of [position, value] pairs
// mix_strat is one of the values of LEDMixStrategy

+ SerialOSCEnc {
	*aliasedRing { |points, mix_strat=0|
		var ring = 0!64;
		points.do({|p|
			var left = p[0].floor % 64,
				right = (left + 1) % 64,
				frac = p[0].frac,
				right_val = (p[1] * frac).floor,
				left_val = (p[1] - right_val);

			switch(mix_strat,
				LEDMixStrategy.greatest, {
					ring[right] = ring[right].max(right_val);
					ring[left] = ring[left].max(left_val);
				},
				LEDMixStrategy.least, {
					ring[right] = ring[right].min(right_val);
					ring[left] = ring[left].min(left_val);
				},
				LEDMixStrategy.sum, {
					ring[right] = ring[right] + right_val;
					ring[left] = ring[left] + left_val;
				},
				LEDMixStrategy.last, {
					ring[right] = right_val;
					ring[left] = left_val;
				});
		});
		^ring
	}

	*ringMapAliased { |n, points, mix_strat=0|
		^SerialOSCEnc.ringMap(n, SerialOSCEnc.aliasedRing(points, mix_strat))
	}
}
