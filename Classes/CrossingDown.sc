// A stream (a) crosses another stream (b) downwards if
// - it was greater than or equal to the second stream in the previous sample
// - and it is lesser than the second stream's current sample
// Alternatively, a becomes lower than b at the same moment when b becomes
// greater than a, so we can just use CrossingUp and swap the inputs

CrossingDown {
	*ar {
		arg a, b;
		^CrossingUp.ar(b,a)
	}

	*kr {
		arg a, b;
		^CrossingUp.kr(b,a)
	}
}
