Crow {
	classvar port;

	*connect {
		port = SerialPort.new(
			"/dev/ttyACM0",
			baudrate: 115200
		);
		port.doneAction = { "crow port closed".postln };
	}

	*disconnect {
		port.close;
	}

	*reset {
		port.putAll("crow.reset()\n");
	}

	// Don't forget your newlines!
	*write {
		arg msg;
		port.putAll(msg);
	}

	// or just use this (marginally slower if you're writing at hz rates)
	*writeln {
		arg msg;
		port.putAll(msg ++ "\n");
	}

	*read {
		var frag, str = "";
		while { frag = port.next; frag != nil } { str = str ++ frag.asAscii; };
		^str
	}
}
